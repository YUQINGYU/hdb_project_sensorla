#include <SoftwareSerial.h>
#include <LowPower.h> // low power sleep
#include <Wire.h>
#include <string.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/interrupt.h>        // Library to use interrupt

#define Timer_wake 2
#define WIFI_Reset 8
#define Done_reply 9
#define WIFI_switch 10

SoftwareSerial WIFI(5,6); //TX/RX //Must declear after MCP331

//Be able to modify
String WIFI_SSID = "Sensorla005"; //Change to WIFI name
String WIFI_PSW = "aweSensorla";   //Change to WIFI password

int countTimeCommand;
boolean found = false;
String comdata = "";

void setup() {
    delay(1000);

    pinMode(Timer_wake, INPUT);
    pinMode(Done_reply, OUTPUT);
    digitalWrite(Done_reply, HIGH);
    delay(5);
    digitalWrite(Done_reply, LOW);
  
    pinMode(WIFI_switch, OUTPUT);
    digitalWrite(WIFI_switch, HIGH);
    delay(100);
    pinMode(WIFI_Reset, OUTPUT);
    delay(100);
    digitalWrite(WIFI_Reset, LOW);
    delay(500);
    digitalWrite(WIFI_Reset, HIGH);
    delay(2000);

    Serial.begin(9600); delay(500);
    Serial.println("power on!");
    delay(100);
    Serial.println("Switch on WIFI Module");
    Serial.println("Changing baud rate to 9600");
    WIFI.begin(115200); 
    delay(500);
    WIFI.println("set uart.baud 0 9600");
    delay(1000);
    WIFI.println("save");
    delay(1000);
    WIFI.println("reboot");
    delay(5000);
    WIFI.end();
    delay(2000);
    WIFI.begin(9600); delay(3000);
    WIFI.flush(); delay(200);
    
    sendCommand("ver",5,"ZEN"); delay(1000);
    sendCommand("set wlan.ssid "+WIFI_SSID,5,"Set OK"); delay(1000);
    sendCommand("set wlan.passkey "+WIFI_PSW,5,"Set OK"); delay(1000);
    sendCommand("set wl o e 1",5,"Set OK"); delay(1000);
    sendCommand("save",5,"Succ"); delay(1000);
    
    WIFI.println("nre");
    comdata = "";
    while (comdata.indexOf("Success") < 0)
    {
      delay(5);
      while (WIFI.available())
      {
        comdata = WIFI.readString();
        comdata.trim();
        Serial.println(comdata);
      }
      if(comdata.indexOf("failed")>0){
        Serial.println("*** Redownload and test again ***");
        delay(100);
        break;
      }
    }
    
    delay(500);
    sendCommand("save",5,"Succ"); 
    delay(600);
    
    Serial.println("Setting Done, now can manually send commands");
    delay(200);

    //Set MCU TX and RX pins as input, for manually key in commands
    pinMode(5, INPUT);
    pinMode(6, INPUT); 
     
    ADCSRA = 0; // disable ADC
    power_adc_disable(); // ADC converter
    SPCR = 0;
    power_spi_disable(); // SPI
    //power_usart0_disable();// Serial (USART)
    //power_timer0_disable();// Timer 0
    power_timer1_disable();// Timer 1
    power_timer2_disable();// Timer 2
    // turn off brown-out enable in software
    MCUCR = bit (BODS) | bit (BODSE);  // turn on brown-out enable select
    MCUCR = bit (BODS);        // this must be done within 4 clock cycles of above
}

void externalTimer()
{
    detachInterrupt(digitalPinToInterrupt(Timer_wake));
    digitalWrite(Done_reply, HIGH);  //delay(8);
    delayMicroseconds(6);
    digitalWrite(Done_reply, LOW);
}

void Going_To_Sleep()
{
    noInterrupts (); // make sure we don't get interrupted before we sleep 
    sleep_enable();  // enables the sleep bit in the mcucr register
    attachInterrupt(digitalPinToInterrupt(Timer_wake),externalTimer,RISING);
    delay(100);
    interrupts(); // interrupts allowed now, next instruction WILL be executed
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    sleep_disable();  // when interrupt is received, sleep mode is disabled and program execution resumes from here
}

void loop() {
    Going_To_Sleep();   // go to sleep - calling sleeping function
}


/*--------------------------------AT commands with timer --------------------------------------*/
boolean sendCommand(String command, int maxTime, char readReplay[]) {
    found = false;
    Serial.print(command);
    delay(100);
    WIFI.println(command);
    while(countTimeCommand < (maxTime*1))
    {
      //WIFI.println(command);
      if(WIFI.find(readReplay))//ok
      {
        found = true;
        break;
      }
      countTimeCommand++;
    }
    
    if(found == true) {
      Serial.println(" => OK");
      countTimeCommand = 0;
    }
    else {
      Serial.println(" => Fail");
      countTimeCommand = 0;
    }
    return found;
}
/*----------------------------------End-------------------------------------*/
