/*
https://docs.zentri.com/zentrios/w/latest/networking-and-security#procedure-for-joining-a-wlan-network
*/
//can add time.zone to check time, then can last more hours
// setup.auto.cmd

#include <SoftwareSerial.h>
#include <LowPower.h> // low power sleep
#include <Wire.h>
#include <string.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/interrupt.h> // Library to use interrupt

#define Timer_wake 2
#define pirPin 3
#define WIFI_Reset 8
#define Done_reply 9
#define WIFI_switch 10

SoftwareSerial WIFI(5,6); //TX/RX //Must declear after MCP331

String WIFI_SSID = "Sensorla001";
String WIFI_PSW = "aweSensorla";
String Static_IP = "192.168.8.106";
String API="P0001";
String MAC;
String Env="&env=3";

int countTimeCommand;
bool found = false;
bool ACK;

String comdata = "";
long unsigned int lowIn;
//the amount of milliseconds the sensor has to be low
//before we assume all motion has stopped
long unsigned int pause = 1000;
boolean lockLow = true;
boolean takeLowTime;

volatile byte Timer_Flag = 0;
volatile byte lastPIRState = 1;  // previous sensor state
volatile byte PIRState = 0;   // current state
volatile byte portState = 0;
byte no_ppl = 0;
byte count=1;

void setup() {
    delay(2000);
    /*
    pinMode(5, INPUT);  //for manually testing use
    pinMode(6, INPUT); 
    */
    pinMode(Timer_wake, INPUT);
    pinMode(Done_reply, OUTPUT);
    digitalWrite(Done_reply, HIGH);
    delay(5);
    digitalWrite(Done_reply, LOW);
  
    delay(100);
    Serial.println("Switch on WIFI");
    pinMode(WIFI_switch, OUTPUT);
    
    digitalWrite(WIFI_switch, HIGH);
    delay(100);
    pinMode(WIFI_Reset, OUTPUT);
    delay(100);
    digitalWrite(WIFI_Reset, LOW);
    delay(500);
    digitalWrite(WIFI_Reset, HIGH);
    delay(2000);
   
    ADCSRA = 0; // disable ADC
    power_adc_disable(); // ADC converter
    SPCR = 0;
    power_spi_disable(); // SPI
    //power_usart0_disable();// Serial (USART)
    //power_timer0_disable();// Timer 0
    power_timer1_disable();// Timer 1
    power_timer2_disable();// Timer 2
    // turn off brown-out enable in software
    MCUCR = bit (BODS) | bit (BODSE);  // turn on brown-out enable select
    MCUCR = bit (BODS);        // this must be done within 4 clock cycles of above
    
    Serial.begin(9600); delay(500);
    WIFI.begin(9600); delay(800);

    String com = "";
    WIFI.flush();
    WIFI.println("get wlan.mac");
    while (!WIFI.available()) {}
    com = WIFI.readString();
    com.trim();
    Serial.print("MAC address:");
    Serial.println(com.substring(12,32));
    delay(300);
    
    //sendCommand("set mdns.enabled 0",5,"Set OK"); delay(500);
    sendCommand("set wl t i " +Static_IP,5,"Set OK"); delay(500); //set wlan.static.ip
    sendCommand("set wl t g 192.168.8.1",5,"Set OK"); delay(500); // set wlan.static.gateway
    sendCommand("set wl t d 0.0.0.0",5,"Set OK"); delay(500); //set wlan.static.dns
    sendCommand("set wl t n 255.255.255.0",5,"Set OK"); delay(500); //set wlan.static.netmask
    sendCommand("set wl d e off",5,"Set OK"); delay(500); //disable DHCP auto obtain an IP 
    sendCommand("nre",5,"Success"); delay(500);
    sendCommand("set system.print_level 2",5,"Set OK"); delay(200);
    sendCommand("save",5,"Success"); delay(500);
    WIFI.println("reboot");
    delay(100);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    delay(200);

    //Serial.println("Configuration...WLAN Join");
    sendCommand("set wl o d 1",3,"Set OK"); delay(200);  // period between auto-join attempts in seconds.
    sendCommand("set wl o r 2",3,"Set OK"); delay(200);  // set wlan.auto_join.retries times - default 3
    sendCommand("set wl j t 3000",3,"Set OK"); delay(200); // set wlan join timeout
    sendCommand("save",5,"Success"); delay(500);
    //WIFI.println("reboot");

    //sendCommand("set sy c m human",5,"Set OK"); delay(200);
    //sendCommand("set system.cmd.echo on",5,"Set OK"); delay(200);
    sendCommand("set http.server.enabled 0",5,"Set OK"); delay(200);
    sendCommand("set http.server.api_enabled 0",5,"Set OK"); delay(200);
    sendCommand("set wlan.powersave.mode 0",5,"Set OK"); delay(200);
    sendCommand("set system.powersave.mode 0",5,"Set OK"); delay(200);
    sendCommand("set wl t 18",5,"Set OK"); delay(200);
    sendCommand("save",5,"Success"); delay(500);
    WIFI.println("reboot");
    delay(200);
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
    delay(500);
    
    sendCommand("set wlan.rssi_average 0",5,"Set OK"); delay(200); //Set to 0 to disable wlan.rssi_average
    sendCommand("set broadcast.interval 0",5,"Set OK"); delay(200);
    sendCommand("set system.indicator.gpio softap -1",2,"Set OK"); delay(200);
    sendCommand("set system.indicator.gpio wlan -1",2,"Set OK"); delay(200);
    sendCommand("set system.indicator.gpio network -1",2,"Set OK"); delay(200);
    sendCommand("save",5,"Success"); delay(500);
    WIFI.println("reboot");
    delay(200);
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
    delay(500);
    
    Serial.println("Config done");
    delay(2500); 

    for (int i = 0; i <= 5; i++) {
      ACK=sendCommand("get wl n s",3,"2");  /* wlan.join.result */
      if(ACK==true){
        delay(100);
        break;
      }
      else{
        Serial.println("network_restart");
        ACK=sendCommand("nre",3,"Suc");
        delay(50);
      }
    }
    
    delay(800);
    WIFI.println("hge http://api.sensorla.co/api/SensorReading/?value="+API+":0"+Env);
    delay(1000);
    sendCommand("read 0 100",3,"Succ");
    delay(200);
    sendCommand("close all",3,"Succ");
    delay(100);
    digitalWrite(WIFI_switch, LOW);
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
    delay(100);

    pinMode(pirPin, INPUT);
    digitalWrite(pirPin, LOW);
    Serial.print("Calibrating sensor ");
    int calibrationTime = 10;
    for (int i = 0; i < calibrationTime; i++) {
      Serial.print(".");
      delay(1000);
    }
    Serial.println(" done");
}

void externalTimer()
{
    //sleep_disable();
    detachInterrupt(digitalPinToInterrupt(Timer_wake));
    digitalWrite(Done_reply, HIGH);  //delay(8);
    delayMicroseconds(6);
    digitalWrite(Done_reply, LOW);
    Timer_Flag = 1;
}

void Going_To_Sleep()
{
    noInterrupts();   // make sure we don't get interrupted before we sleep 
    sleep_enable();    // / enables the sleep bit in the mcucr register
    attachInterrupt(digitalPinToInterrupt(Timer_wake),externalTimer,RISING);
    attachInterrupt(digitalPinToInterrupt(pirPin), wakeUpNow, CHANGE);
    delay(100);
    interrupts();   // interrupts allowed now, next instruction WILL be executed
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    sleep_disable();     // when interrupt is received, sleep mode is disabled and program execution resumes from here
    detachInterrupt(pirPin);   // detach interrupt to avoid further interrupts until our ISR is finished
}

void wakeUpNow() {
    //Interrupt service routine or ISR
    PIRState = !lastPIRState;
}

void upload(){
    Serial.print("Send:"); Serial.println(no_ppl);
    char readReplay[]="";
    int countTimeCommand = 0;
    String APIstr="hge http://api.sensorla.co/api/SensorReading/?value="+API+":"+String(no_ppl)+Env;
    signed long oldtime=millis();
    digitalWrite(WIFI_switch, HIGH); 
    delay(1500);
   
    for (int i = 0; i <= 3; i++) {
      ACK=sendCommand("get wl n s",1,"2");  //wlan.join.result 
      if(ACK==true){
          WIFI.flush();
          delay(300);
          sendCommand(APIstr,2,"Open");
          //WIFI.println(APIstr);
          delay(900);

          if(count>=4){
              delay(100);
              sendCommand("close all",3,"Suc");
              count=0;
          }
          break;
      }
      else{
          if(i==3){
            sendCommand("reboot",2,"Associated");
            delay(300);
          }
          else{
            sendCommand("nre",2,"Suc"); delay(100);
          }
        }
      }
      delay(100);
      digitalWrite(WIFI_switch, LOW);
      int timeTake=0;
      timeTake=millis()-oldtime;
      Serial.println(timeTake);
}


void loop() {
  interrupts();    // enable interrupts
  if (PIRState != lastPIRState) {
    if (PIRState == 0)
    {
      delay(50); //Serial.println(PIRState);   // read status of interrupt pin
    }
    else  { // PIRState == lastPIRState
      no_ppl = no_ppl + 1;
      Serial.print("Total ppl:");    // enable for debugging
      Serial.println(no_ppl);
      delay(50);
    }
  }
  lastPIRState = PIRState;
  delay(50);
  if(Timer_Flag==1)
    {
      upload();
      count=count+1;
      no_ppl = 0;
      Timer_Flag = 0;
      Serial.flush();
      WIFI.flush();
    }
  Going_To_Sleep();   // go to sleep - calling sleeping function
}


/*--------------------------------AT commands with timer --------------------------------------*/
boolean sendCommand(String command, int maxTime, char readReplay[]) {
    found = false;
    Serial.print(command);
    delay(30);
    WIFI.println(command);
    while(countTimeCommand < (maxTime*1))
    {
      //WIFI.println(command);
      if(WIFI.find(readReplay))//ok
      {
        found = true;
        break;
      }
      countTimeCommand++;
    }
    
    if(found == true) {
      Serial.println(" => OK");
      countTimeCommand = 0;
    }
    else {
      Serial.println(" => Fail");
      countTimeCommand = 0;
    }
    return found;
}
/*----------------------------------End-------------------------------------*/
