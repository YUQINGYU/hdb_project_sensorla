/*
https://docs.zentri.com/zentrios/w/latest/networking-and-security#procedure-for-joining-a-wlan-network
*/
//can add time.zone to check time, then can last more hours

#include <SoftwareSerial.h>
#include <LowPower.h> // low power sleep
#include <Wire.h>
#include <string.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/interrupt.h> // Library to use interrupt
#include "MCP3221.h"
#include "LMP91000.h"

#define AmpGain 350000.00
#define NH3_SENSITIVITY 0.16

#define Timer_wake 2
#define WIFI_Reset 8
#define Done_reply 9
#define WIFI_switch 10

const byte DEV_ADDR = 0x4D;   // I2C address of the MCP3221 (Change as needed)
LMP91000 lmp91000;
MCP3221 mcp3221(DEV_ADDR);
SoftwareSerial WIFI(5,6); //TX/RX Must declear after MCP331

String WIFI_SSID = "Sensorla005";
String WIFI_PSW = "aweSensorla";
String Static_IP = "192.168.8.107";
String API="A0005";
String MAC;
String Env="&env=14";
const int I2C_SW =A3;

int countTimeCommand;
bool found = false;
bool ACK;

String comdata = "";
volatile byte Timer_Flag = 0;
float old_ammonia=0.00;
float NH3_ppm=0.00;
byte count=1;

void setup() {
    delay(1000);
    pinMode(Timer_wake, INPUT);
    pinMode(Done_reply, OUTPUT);
    digitalWrite(Done_reply, HIGH);
    delay(5);
    digitalWrite(Done_reply, LOW);

    pinMode(I2C_SW, OUTPUT);
    digitalWrite(I2C_SW, HIGH);

    Serial.begin(9600); delay(500);
        
    pinMode(WIFI_switch, OUTPUT);
    digitalWrite(WIFI_switch, HIGH);
    delay(500);
    pinMode(WIFI_Reset, OUTPUT);
    delay(100);
    digitalWrite(WIFI_Reset, LOW);
    delay(500);
    digitalWrite(WIFI_Reset, HIGH);
    delay(2000);
    
    ADCSRA = 0; // disable ADC
    power_adc_disable(); // ADC converter
    SPCR = 0;
    power_spi_disable(); // SPI
    power_timer1_disable();// Timer 1
    power_timer2_disable();// Timer 2
    // turn off brown-out enable in software
    MCUCR = bit (BODS) | bit (BODSE);  // turn on brown-out enable select
    MCUCR = bit (BODS);        // this must be done within 4 clock cycles of above
    
    Serial.println("Switch on WIFI"); delay(100);

    Wire.begin(); // wake up I2C bus
    delay(1000);
    uint8_t res = lmp91000.configure( 
            LMP91000_TIA_GAIN_350K | LMP91000_RLOAD_10OHM,
            LMP91000_REF_SOURCE_EXT | LMP91000_INT_Z_20PCT 
                  | LMP91000_BIAS_SIGN_POS | LMP91000_BIAS_0PCT,
            LMP91000_FET_SHORT_DISABLED | LMP91000_OP_MODE_AMPEROMETRIC                  
      );

    delay(200);
    Serial.print("Config Result: ");
    Serial.println(res); 
    /*
    Serial.print("STATUS: ");
    Serial.println(lmp91000.read(LMP91000_STATUS_REG),HEX);
    Serial.print("TIACN: ");
    Serial.println(lmp91000.read(LMP91000_TIACN_REG),HEX);
    Serial.print("REFCN: ");
    Serial.println(lmp91000.read(LMP91000_REFCN_REG),HEX);
    Serial.print("MODECN: ");
    Serial.println(lmp91000.read(LMP91000_MODECN_REG),HEX);
    delay(200);
    */
    
    mcp3221.setVref(3000);
    mcp3221.setSmoothing(ROLLING_AVG);
    mcp3221.setAlpha(134);
    mcp3221.setNumSamples(16);
    delay(500);
    
    WIFI.begin(9600); delay(800);
    
    String com = "";
    WIFI.flush();
    WIFI.println("get wlan.mac");
    while (!WIFI.available()) {}
    com = WIFI.readString();
    com.trim();
    Serial.print("MAC address:");
    Serial.println(com.substring(12,32));
    delay(300);
    
    //sendCommand("set mdns.enabled 0",5,"Set OK"); delay(500);
    sendCommand("set wl t i " +Static_IP,5,"Set OK"); delay(500); //set wlan.static.ip
    sendCommand("set wl t g 192.168.8.1",5,"Set OK"); delay(500); // set wlan.static.gateway
    sendCommand("set wl t d 0.0.0.0",5,"Set OK"); delay(500); //set wlan.static.dns
    sendCommand("set wl t n 255.255.255.0",5,"Set OK"); delay(500); //set wlan.static.netmask
    sendCommand("set wl d e off",5,"Set OK"); delay(500); //disable DHCP auto obtain an IP 
    sendCommand("nre",5,"Success"); delay(500);
    sendCommand("set system.print_level 2",5,"Set OK"); delay(200);
    sendCommand("save",5,"Success"); delay(500);
    WIFI.println("reboot");
    delay(100);
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    delay(200);

    //Serial.println("Configuration...WLAN Join");
    sendCommand("set wl o d 1",3,"Set OK"); delay(200);  // period between auto-join attempts in seconds.
    sendCommand("set wl o r 2",3,"Set OK"); delay(200);  // set wlan.auto_join.retries times - default 3
    sendCommand("set wl j t 3000",3,"Set OK"); delay(200); // set wlan join timeout
    sendCommand("save",5,"Success"); delay(500);
    //WIFI.println("reboot");

    //sendCommand("set sy c m human",5,"Set OK"); delay(200);
    //sendCommand("set system.cmd.echo on",5,"Set OK"); delay(200);
    sendCommand("set http.server.enabled 0",5,"Set OK"); delay(200);
    sendCommand("set http.server.api_enabled 0",5,"Set OK"); delay(200);
    sendCommand("set wlan.powersave.mode 0",5,"Set OK"); delay(200);
    sendCommand("set system.powersave.mode 0",5,"Set OK"); delay(200);
    sendCommand("set wl t 18",5,"Set OK"); delay(200);
    sendCommand("save",5,"Success"); delay(500);
    WIFI.println("reboot");
    delay(200);
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
    delay(500);
    
    sendCommand("set wlan.rssi_average 0",5,"Set OK"); delay(200); //Set to 0 to disable wlan.rssi_average
    sendCommand("set broadcast.interval 0",5,"Set OK"); delay(200);
    sendCommand("set system.indicator.gpio softap -1",2,"Set OK"); delay(200);
    sendCommand("set system.indicator.gpio wlan -1",2,"Set OK"); delay(200);
    sendCommand("set system.indicator.gpio network -1",2,"Set OK"); delay(200);
    sendCommand("save",5,"Success"); delay(500);
    WIFI.println("reboot");
    delay(200);
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
    delay(500);
    
    Serial.println("Config done");
    delay(2500); 

    for (int i = 0; i <= 5; i++) {
      ACK=sendCommand("get wl n s",3,"2");  /* wlan.join.result */
      if(ACK==true){
        delay(100);
        break;
      }
      else{
        Serial.println("network_restart");
        ACK=sendCommand("nre",3,"Suc");
        delay(50);
      }
    }
    
    delay(800);
    WIFI.println("hge http://api.sensorla.co/api/SensorReading/?value="+API+":"+"0.001"+Env);
    delay(1000);
    sendCommand("read 0 100",3,"Succ");
    delay(200);
    sendCommand("close all",3,"Succ");
    delay(100);
    digitalWrite(WIFI_switch, LOW);
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
    delay(100);
}

void externalTimer()
{
    sleep_disable();
    detachInterrupt(digitalPinToInterrupt(Timer_wake));
    digitalWrite(Done_reply, HIGH);  //delay(8);
    delayMicroseconds(6);
    digitalWrite(Done_reply, LOW);
}

void Going_To_Sleep()
{
    noInterrupts();   // make sure we don't get interrupted before we sleep 
    sleep_enable();    // / enables the sleep bit in the mcucr register
    attachInterrupt(digitalPinToInterrupt(Timer_wake),externalTimer,RISING);
    delay(100);
    interrupts();   // interrupts allowed now, next instruction WILL be executed
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
}

void upload(){
    char readReplay[]="";
    int countTimeCommand = 0;
    String APIstr="hge http://api.sensorla.co/api/SensorReading/?value="+API+":"+String(NH3_ppm,3)+Env;
    //delay(100);
    //Serial.println(APIstr);
    signed long oldtime=millis();
    digitalWrite(WIFI_switch, HIGH); 
    delay(1500);
    
    for (int i = 0; i < 3; i++) {
      ACK=sendCommand("get wl n s",1,"2");  //wlan.join.result 
      if(ACK==true){
          WIFI.flush();
          delay(300);
          sendCommand(APIstr,2,"Open");
          //WIFI.println(APIstr);
          delay(900);
          //sendCommand("read 0 100",2,"Suc");

          if(count>=4){
              sendCommand("close all",2,"Suc");
              count=0;
          }
          break;
      }
      else{
          if(i==3){
            sendCommand("reboot",2,"Associated");
            delay(300);
          }
          else{
            sendCommand("nre",2,"Suc"); delay(400);
          }
        }
      }
      delay(100);
      digitalWrite(WIFI_switch, LOW);
      int timeTake=0;
      timeTake=millis()-oldtime;
      Serial.println(timeTake);
}

void Sensor()
{
     unsigned int reading[3];   //Maybe need to Change to float
     float tmpf,ave,Vmid=590.00,MGS;
     float total;
     
     Serial.println("Measuring...");
     power_twi_enable();
     delay(100); //for recovery
     for (int i=0; i <= 2; i++){ 
          reading[i]=mcp3221.getVoltage();
          total = total + reading[i];
          LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
          delay(200); //for recovery
     }
     ave = total/3.00;
     tmpf = ave - Vmid;
     if(tmpf < 0) 
          tmpf =0.00;
          tmpf = tmpf * 1000.00;   // change to uV
          tmpf = (float)tmpf / AmpGain;  // change to uA
          NH3_ppm = (tmpf / NH3_SENSITIVITY);
     
     Serial.println(NH3_ppm,3);
     delay(100);
     power_twi_disable();
     delay(100);
}
void loop() {
     Sensor();
     upload();
     Serial.println("Go to sleep");
     delay(100);
     Going_To_Sleep();   // go to sleep - calling sleeping function
}


/*--------------------------------AT commands with timer --------------------------------------*/
boolean sendCommand(String command, int maxTime, char readReplay[]) {
    found = false;
    Serial.print(command);
    delay(30);
    WIFI.println(command);
    while(countTimeCommand < (maxTime*1))
    {
      //WIFI.println(command);
      if(WIFI.find(readReplay))//ok
      {
        found = true;
        break;
      }
      countTimeCommand++;
    }
    
    if(found == true) {
      Serial.println(" => OK");
      countTimeCommand = 0;
    }
    else {
      Serial.println(" => Fail");
      countTimeCommand = 0;
    }
    return found;
}
/*----------------------------------End-------------------------------------*/
